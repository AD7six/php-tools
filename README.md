#PHP Tools

Might become more, right now it's one tool.

##Convert 54

a cli tool for converting to use php 5.4 short array syntax

Usage:

	php convert54 path/to/file/or/dir

This will find for example:

	$foo = array('x' => 'y');

and replace it with:

	$foo = ['x' => 'y'];

It will lint-check files after conversion - and if there's an error encountered - revert the failed file and halt.
