<?php
namespace AD7six\PHPTools;

class Convert54 {

	protected static $_config = array(
		'backupFileMask' => ':fullpath:.bak',
		'updatedFileMask' => ':fullpath:',
		'extensions' => array(
			'php',
			'ctp'
		)
	);

	protected static $_results = [
		'characters' => 0,
		'lines' => 0,
		'replacements' => 0
	];

	public static function process($path) {
		if (is_array($path)) {
			foreach($path as $p) {
				static::process($p);
			}
			return static::$_results;
		}
		if (is_dir($path)) {
			return static::processDir($path);
		}
		return static::processFile($path);
	}

	public static function processDir($dir) {
		$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir));

		foreach($iterator as $path) {
			if ($path->isDir()) {
				continue;
			}

			$extension = $path->getExtension();
			if (!in_array($extension, static::$_config['extensions'])) {
				continue;
			}

			static::processFile($path->getRealPath());
		}

		return static::$_results;
	}

	public static function processFile($file) {
		echo "Processing $file\n";

		exec('php -l ' . escapeshellarg($file), $output, $return);
		if ($return) {
			echo "\tParse errors found - skipping\n";
			return false;
		}

		$substitutions = [
			':fullpath:' => $file,
			':tmpfile:' => str_replace(array('/', '\\'), '_', $file),
			':file:' => basename($file),
		];
		$backupFile = static::_filename(static::$_config['backupFileMask'], $substitutions);
		$writeFile = static::_filename(static::$_config['updatedFileMask'], $substitutions);

		if ($backupFile && !file_exists($backupFile)) {
			copy($file, $backupFile);
		}

		$contents = static::processString(file_get_contents($file));

		if ($writeFile) {
			file_put_contents($writeFile, $contents);
			exec('php -l ' . escapeshellarg($writeFile), $output, $return);
			if ($return) {
				echo "\tParse errors found - reverting\n";
				copy($file, str_replace('.php', '.fail', $file));
				static::revertFile($file);
				die();
				return false;
			}
		}
	}

	public static function processString($contents) {
		$tokens = token_get_all($contents);

		$pos = 0;
		$i = 0;
		$max = count($tokens);

		while ($i < $max) {
			$data = $tokens[$i];

			$token = is_string($data) ? 0 : $data[0];
			$string = is_string($data) ? $data : $data[1];
			$stringLength = strlen($string);

			if ($token === T_ARRAY) {
				$j = $i;
				$fullString = $string;

				$isArray = false;
				do {
					$j++;
					$next = $tokens[$j];
					$nextString = is_string($next) ? $next : $next[1];
					$fullString .= $nextString;
					if (trim($nextString)) {
					   	if ($nextString === '(') {
							$isArray = true;
						}
						break;
					}
				} while($tokens[$j]);

				if ($isArray) {
					$length = strlen($fullString);
					$contents = substr_replace($contents, '[', $pos, $length);
					$stringLength = 1;
					$i = $j;

					$nesting = 0;
					$offset = 0;
					do {
						$j++;
						$next = $tokens[$j];
						$nextString = is_string($next) ? $next : $next[1];
						$offset += strlen($nextString);
						$nextToken = is_string($next) ? 0 : $next[0];

						if ($nextString === '(') {
							$nesting++;
						} elseif ($nextString === ')') {
							if ($nesting === 0) {
								$contents = substr_replace($contents, ']', $pos + $offset, 1);
								break;
							}
							$nesting--;
						}
					} while($tokens[$j]);
				}
			}

			$pos += $stringLength;
			$i++;
		}

		return $contents;
	}

	public static function reset() {
		static::$_results = [
			'characters' => 0,
			'lines' => 0,
			'replacements' => 0
		];
	}

	public static function results() {
		return static::$_results;
	}

	public static function revert($path) {
		if (is_array($path)) {
			foreach($path as $p) {
				static::revert($p);
			}
			return static::$_results;
		}
		if (is_dir($path)) {
			return static::revertDir($path);
		}
		return static::revertFile($path);
	}

	public static function revertDir($dir) {
		$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir));

		foreach($iterator as $path) {
			if ($path->isDir()) {
				continue;
			}

			$extension = $path->getExtension();
			if (!in_array($extension, static::$_config['extensions'])) {
				continue;
			}

			static::revertFile($path->getRealPath());
		}

		return static::$_results;
	}

	public static function revertFile($file) {
		$substitutions = [
			':fullpath:' => $file,
			':tmpfile:' => str_replace(array('/', '\\'), '_', $file),
			':file:' => basename($file),
		];
		$backupFile = static::_filename(static::$_config['backupFileMask'], $substitutions);

		if ($backupFile && file_exists($backupFile)) {
			echo "restoring $file\n";
			rename($backupFile, $file);
		}
	}

	protected static function _filename($mask, $replacements) {
		if (!$mask) {
			return false;
		}
		return str_replace(array_keys($replacements), $replacements, $mask);
	}

}
